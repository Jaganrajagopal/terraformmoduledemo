variable "region" {
  description = "The AWS region to deploy resources"
  type        = string
}
variable "instance_type" {
    description = "instance type"
    #default = "t2.micro"
    type = string
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  #type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_name" {
  description = "The name of the VPC"
  type        = string
}

variable "public_subnets" {
  description = "List of CIDR blocks for public subnets"
  type        = list(string)
}

variable "azs" {
  description = "List of availability zones"
  type        = list(string)
}
