provider "aws" {
  region = "us-west-1"
}

resource "aws_instance" "ec2_instance" {
    ami = "ami-06c68f701d8090592"
    instance_type = var.instance_type    
    tags = {
    Name = "jaganinstance"

  }
}
resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "public" {
  count             = length(var.public_subnets)
  vpc_id            = aws_vpc.this.id
  cidr_block        = var.public_subnets[count.index]
  availability_zone = element(var.azs, count.index)
  tags = {
    Name = "${var.vpc_name}-public-${count.index}"
  }
}

