provider "aws" {
  region = var.region
}

module "vpc" {
  source = "./modules/vpc"
  instance_type = var.instance_type
  region = var.region
  vpc_name = var.vpc_name
  public_subnets = var.public_subnets
  vpc_cidr = var.vpc_cidr
  azs = var.azs
  
  
}

# output "vpc_id" {
#   value = module.vpc.vpc_id
# }

# output "public_subnet_ids" {
#   value = module.vpc.public_subnet_ids
# }

# output "security_group_id" {
#   value = module.vpc.security_group_id
# }
