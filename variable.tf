# region = "us-west-1"
# vpc_cidr = "10.0.0.0/16"
# vpc_name = "my-vpc"
# public_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
# azs = ["us-west-1a", "us-west-1b"]
variable "instance_type" {
    description = "instance type"
    default = "t2.micro"
    type = string
}
variable "region" {
  description = "The region where resources will be deployed"
  type        = string
}
variable "public_subnets" {
  description = "The public subnet "
  type        = list(string)
  default = [ "10.0.1.0/24", "10.0.2.0/24" ]
  
}
variable "vpc_name" {
  description = "The public subnet "
  type        = string
  default = "my-vpc"
}

variable "vpc_cidr" {
  description = "The public subnet "
  type        = string
  default = "10.0.0.0/16"
}
variable "azs" {
  description = "The public subnet "
  type        = list(string)
  default = ["us-east-1a", "us-east-1b"]
}


